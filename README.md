## Network Lab1 P2P

This project is for CSE514 lab1.

### Todo List

* ~~Use Ant to build and run the project~~
* Chunk and File
* Connection
* Command line UI
* Server to Peer Protocol
* Peer to Peer Protocol
* Stabalize and Failure
* P2P optimization


### How to compile and run the program

To compile the project, __cd__ to the project root folder, and execute:

> ant comp

To run the project, __cd__ to the project root folder, and execute:

> ant run

After running the project, you can clean the project by running:

> ant clean

Note that Apache Ant must be installed first in order to run __ant__.







